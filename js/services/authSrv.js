app.service("AuthSrv", function($http) {
  'use strict';

  return({
    login: login,

  });

  function login(user, password) {
    var request = $http({
      method: "post",
      url: "/login",
      data: {
        username: user,
        password: password
      }
    });
    return( request.then(handleSuccess, handleError) );
  }

  function handleSuccess(response) {
    return(response);
  }

  function handleError(response) {
    return(response);
  }

});
