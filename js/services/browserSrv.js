app.service("BrowserSrv", function($http, $location, $cookies) {
  'use strict';

  return({
    getDocuments: getDocuments,
    getDocument : getDocument,
    findText    : findText
  });

  function getDocuments() {
    var request = $http({
      method: "get",
      url: "/documents",
      params: {
      }
    });
    return( request.then(handleSuccess, handleError) );
  }

  function getDocument(id) {
    var request = $http({
      method: "get",
      url: "/document/" + id + "/text",
      params: {
      }
    });
    return( request.then(handleSuccess, handleError) );
  }

  function findText(id, query){
    var request = $http({
      method: "get",
      url: "/document/" + id + "/text",
      params: {
        search: query
      }
    });
    return( request.then(handleSuccess, handleError) );
  }

  function handleSuccess(response) {
    return(response);
  }

  function handleError(response) {
    return(response);
  }

});

