app.component('documentsItem', {

  templateUrl: '/static/partials/documents_item.cmp.html',
  bindings: {
    doc: '<', //one way data binding
    onClick: '&',
    selected: '<'
  }

});
