app.component('browserHeader', {

    templateUrl: '/static/partials/header.cmp.html',
    controller: function($location) {
      var hc = this;
      hc.logoutUser = function(){
        $location.path("/logout");
      }
    }

});
