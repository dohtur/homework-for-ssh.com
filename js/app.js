
var app = angular.module('docBrowser', ['ui.router', 'ngSanitize', 'ngCookies'])
  .config(function($stateProvider, $urlRouterProvider) {
  'use strict';

  $urlRouterProvider.otherwise('/login');
  $stateProvider
    .state('login', {
      url: '/login',
      controller: 'AuthCtrl',
      templateUrl: '/static/partials/login.tpl.html'
    })
    .state('logout', {
      url: '/logout',
      controller: function($scope, $location, $cookies){
                    // clear cached data
                    localStorage.removeItem('document');
                    localStorage.removeItem('documents');
                    localStorage.removeItem('selected');

                    $cookies.remove('token');
                    $location.path('/login');
                  }
    })
    .state('browser', {
      url: '/browser',
      controller: 'BrowserCtrl as bc',
      templateUrl: '/static/partials/browser.tpl.html'
    });
  }
).factory('api', function ($http, $cookies) {
  return {
    init: function (token) {
      var tk = token || $cookies.get('token');
      $http.defaults.headers.common['Authorization'] = 'JWT ' + tk;
    }
  }
});

app.run(function (api) {
  api.init();
});

