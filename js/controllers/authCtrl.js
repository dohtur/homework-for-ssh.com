app.controller('AuthCtrl', function ($scope, $location, $cookies, $timeout, api, AuthSrv) {
  'use strict';

  $scope.user     = '';
  $scope.password = '';
  $scope.message  = null;

  $timeout(function(){
    angular.element('#user').focus();
  }, 0);

  $scope.loginUser = function(ev) {
    if(!ev || ev.which === 13){
      AuthSrv.login($scope.user, $scope.password)
        .then(function(data){
          if(data.status === 200) {
            var token = data.data.token;
            $cookies.put("token", token);

            // put token to header of each request
            api.init(token);

            $location.path("browser");
          } else {
            $scope.message = data.statusText;
          }
        });
    }
  }

});
