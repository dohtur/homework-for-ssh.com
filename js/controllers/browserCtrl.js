app.controller('BrowserCtrl', function($scope, $sce, $location, $cookies, BrowserSrv){
  'use strict';

  var bc = this;

  bc.documents   = [];
  bc.document    = localStorage.document ? JSON.parse(localStorage.document) : null; // restore document from cache if page reloaded
  bc.selected    = localStorage.selected ? parseInt(localStorage.selected) : null;   // restore document selection from cache if if page reloaded

  bc.searchQuery    = ''; // search query for text
  bc.wordCounter    = 0;  // counter for findText result
  bc.searchMessage  = ''; // Message for in text search

  bc.searchQueryDoc = ''; // search query for documents
  bc.searchMessageDoc = ''; // Message for documents search

  // Total counters
  bc.total       = 0; // documents
  bc.totalF      = 0; // filtered documents
  bc.totalSize   = 0; // documents size
  bc.totalSizeF  = 0; // size of filtered documents

  /**
   * Calculate stats for bc.documents and documents search.
   * @param  {boolean} filtered - if true - bc.documents contain filtered data and counters must be recalculated.
   *
   */
  function calcTotals(filtered){
    bc.total     = JSON.parse(localStorage.documents).length;
    bc.totalSize  = JSON.parse(localStorage.documents).reduce(function(ttl, doc) { return ttl + doc.size}, 0);

    if(filtered){
      bc.totalF     = bc.documents.length;
      bc.totalSizeF = bc.documents.reduce(function(ttl, doc) { return ttl + doc.size}, 0);
    } else {
      bc.totalF     = bc.total;
      bc.totalSizeF = bc.totalSize;
    }
    bc.searchMessageDoc = "documents: " + bc.totalF + " of " + bc.total + ", size: " + bc.totalSize + " of " + bc.totalSizeF;
  }

  /**
   * Load documents from API.
   *
   */
  function loadDocuments() {
    BrowserSrv.getDocuments().then(
      function(data) {
        if(data.status === 200){
          bc.documents = data.data;
          localStorage.setItem("documents", JSON.stringify(data.data));

          calcTotals(false);

        } else {
          $location.path("/logout");
        }
      }
    );
  }

  /**
   * Getting document data from API.
   * @param  {int} index - element position in bc.documents array.
   *
   */
  bc.getDocument = function(index){
    var doc = bc.documents[index];
    BrowserSrv.getDocument(doc.id).then(
      function(data) {
        if(data.status === 200){
          bc.document = {meta: doc, text: data.data};
          localStorage.setItem("document", JSON.stringify(bc.document));

          bc.selected = doc.id;
          localStorage.setItem("selected", doc.id);

          bc.resetSearch(false);
        } else {
          $location.path("/logout");
        }
      }
    );

  }

  /**
   * Ask server if query string in current text then marks each entry with tags for highlight.
   * Assuming that only "document browser" able to inject tags for marking purpose, in other cases - refactor.
   * @param  {event} ev - keyboard event from text search field
   *
   */
  bc.findText = function(ev){
    if(!ev || ev.which === 13){
      if(bc.searchQuery === ''){
        bc.resetSearch(true);
      } else {
        bc.document = JSON.parse(localStorage.document);
        BrowserSrv.findText(bc.selected, bc.searchQuery.replace(/[-\\.,_*+?^$[\](){}!=|]/ig, "\\$&")).then(
          function(data){
            if(data.status === 200){
              if(data.data.length > 0){
                bc.wordCounter = data.data.length;
                bc.searchMessage = "We found <strong>" + data.data.length + "</strong> results.";
                var text = bc.document.text;
                bc.document.text = text.replace(new RegExp(bc.searchQuery, 'g'), '<span class="query">' + bc.searchQuery + '</span>');
              } else {
                bc.searchMessage = "Nothing found.";
              }
            } else {
              $location.path("/logout");
            }
          }
        );
      }
    }
  }

  /**
   * Filter documents.
   * If document in filtered set he is remains opened with selected state, else - selection off, text -off.
   *
   */
  bc.findDocument = function(){
    if(bc.searchQueryDoc !== ''){
      bc.documents = JSON.parse(localStorage.documents).filter(function(doc){
                    return doc.name.toLowerCase().indexOf(bc.searchQueryDoc.toLowerCase()) > -1;
                  });
      var exist = bc.documents.filter(function(doc){ return doc.id === bc.selected });

      if(exist.length === 0){
        resetDocument();
      }
    } else {
      bc.documents = JSON.parse(localStorage.documents); //restore documents list
      resetDocument(); // reset selection
    }

    calcTotals(true);
  }

  /**
   * Reset search for text.
   * @param  {boolean} restore - if true - restore original document from cache w/o marks setted by findText().
   *
   */
  bc.resetSearch = function(restore){
    bc.searchQuery = '';
    bc.wordCounter = 0;
    bc.searchMessage = '';
    if(restore){
      bc.document = JSON.parse(localStorage.document);
    }
  }

  /**
   * Reset selected document and clear cache.
   *
   */
  function resetDocument(){
    bc.document = null;
    bc.selected = null;
    localStorage.removeItem("document");
    localStorage.removeItem("selected");

  }

  loadDocuments();

});
