var del        = require('del'),
    gulp       = require('gulp'),
    gutil      = require('gulp-util'),
    uglify     = require('gulp-uglify'),
    sass       = require('gulp-sass'),
    cssnano    = require('gulp-cssnano'),
    concat     = require('gulp-concat'),
    ngAnnotate = require('gulp-ng-annotate'),
    inject     = require('gulp-inject'),
    serve      = require('gulp-serve');

gulp.task('clean', function(cb) {
  del(['./dist', './dev'], cb);
});

gulp.task('sass', function () {
  gulp.src('./css/src/*.scss')
    .pipe(sass())
    .pipe(cssnano())
    .pipe(gulp.dest('./dist'));
});

gulp.task('js', function() {
  gulp.src(['js/**/app.js', 'js/**/*.js'])
    .pipe(concat('bundle.js'))
    .pipe(ngAnnotate())
    .pipe(uglify())
    .pipe(gulp.dest('./dist'));
});

gulp.task('index', function(){
    var target = gulp.src('./index.html');
    var sources = gulp.src(['./dist/bundle.js', './dist/styler.css'], {read: false});
    return target
                .pipe(inject(sources,  {ignorePath: 'dist', addRootSlash: false, addPrefix: '/static'}))
                .pipe(gulp.dest('./dist'));
});

gulp.task('partials', function(){
    return gulp.src('./partials/*')
            .pipe(gulp.dest('./dist/partials'));
});

gulp.task('build', ['clean', 'sass', 'js', 'index', 'partials']);

gulp.task('index-dev', function(){
    var target = gulp.src('./index.html');
    var sources = gulp.src(['./dist/bundle.js', './dist/styler.css'], {read: false});
    return target
                .pipe(inject(sources,  {ignorePath: 'dist', addRootSlash: false}))
                .pipe(gulp.dest('./dev'));
});

gulp.task('partials-dev', function(){
    return gulp.src('./partials/*')
            .pipe(gulp.dest('./dev/partials'));
});

gulp.task('serve', serve({
    root: ['dev', 'dist'],
    port: 3000
}));

gulp.task('build-dev', ['clean', 'sass', 'js', 'index-dev', 'partials-dev']);
